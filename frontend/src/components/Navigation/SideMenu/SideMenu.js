import React from "react";

import NavigationItem from '../NavigationItems/NavigationItem/NavigationItem'
import classes from './SideMenu.module.css'

const SideMenu = () => {
  return (
   
      <nav className={`${classes.Sidenav} 'navbar' 'col-sm-1'`} >
        <ul
          className="nav nav-pills flex-column "
          data-spy="affix"
          data-offset-top="60"
        >
          <NavigationItem link='/'>Section 1</NavigationItem>
          <NavigationItem link='/'>Section 2</NavigationItem>
          <NavigationItem link='/'>Section 3</NavigationItem>
          <NavigationItem link='/'>Section 4</NavigationItem>
          <NavigationItem link='/'>Section 5</NavigationItem>
          <NavigationItem link='/'>Section 6</NavigationItem>
          <NavigationItem link='/'>Section 7</NavigationItem>
          <NavigationItem link='/'>Section 8</NavigationItem>
        </ul>
      </nav>
      
  );
};

export default SideMenu;
