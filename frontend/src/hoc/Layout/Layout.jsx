import React,{useState} from "react";

import classes from "./Layout.module.css";
import Toolbar from "../../components/Navigation/Toolbar/Toolbar";
import SideDrawer from "../../components/Navigation/SideDrawer/SideDrawer";
import Footer from '../../components/Footer/Footer'
//import SideMenu from '../../components/Navigation/SideMenu/SideMenu'

 const Layout=(props)=> {
  const [showSideDrawer,setShowSideDrawer]=useState(false)

  const sideDrawerClosedHandler = () => {
   setShowSideDrawer(false)
  };

  const sideDrawerToggleHandler = () => {
   setShowSideDrawer(!showSideDrawer)
  };

 
    return (
      <React.Fragment>
        <Toolbar drawerToggleClicked={sideDrawerToggleHandler} />
        <SideDrawer
          open={showSideDrawer}
          closed={sideDrawerClosedHandler}
        />
        <main className={classes.Content}>{props.children}</main>
        <Footer/>
      </React.Fragment>
    );

}

export default Layout;
