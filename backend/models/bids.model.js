const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const bidsSchema = new Schema({
  username: { type: String, required: true },
  itemid: { type: String, required: true },
  bidprice: { type: Number, required: true },
  date: { type: Date, required: true },
},
{
  timestamps: true,
});

const Bids = mongoose.model('Bids', bidsSchema);

module.exports = Bids;